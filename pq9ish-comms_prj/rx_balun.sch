<Qucs Schematic 0.0.20>
<Properties>
  <View=-63,85,884,1155,0.992608,0,0>
  <Grid=10,10,1>
  <DataSet=rx_balun.dat>
  <DataDisplay=rx_balun.dpl>
  <OpenDisplay=1>
  <Script=rx_balun.m>
  <RunScript=0>
  <showFrame=0>
  <FrameText0=Title>
  <FrameText1=Drawn By:>
  <FrameText2=Date:>
  <FrameText3=Revision:>
</Properties>
<Symbol>
</Symbol>
<Components>
  <Pac P2 1 80 300 -103 -26 1 1 "1" 1 "100 Ohm" 1 "0 dBm" 0 "1 GHz" 0 "26.85" 0>
  <L L3 1 130 430 10 -26 0 1 "100 nH" 1 "" 0>
  <L L4 1 130 180 10 -26 0 1 "100 nH" 1 "" 0>
  <C C5 1 190 210 -26 17 1 2 "4.3 pF" 1 "" 0 "neutral" 0>
  <C C4 1 190 400 -26 -55 0 2 "4.3 pF" 1 "" 0 "neutral" 0>
  <GND * 5 130 150 0 0 0 2>
  <GND * 5 130 460 0 0 0 0>
  <L L5 1 270 400 -26 10 1 2 "43 nH" 1 "" 0>
  <L L6 1 270 210 -26 -48 1 0 "43 nH" 1 "" 0>
  <GND *2 5 750 390 0 0 0 0>
  <Pac P1 1 750 360 18 -26 0 1 "2" 1 "50 Ohm" 1 "0 dBm" 0 "1 GHz" 0 "26.85" 0>
  <C C7 1 330 180 17 -26 0 1 "5.6 pF" 1 "" 0 "neutral" 0>
  <C C6 0 330 430 17 -26 0 1 "5.6 pF" 1 "" 0 "neutral" 0>
  <GND * 5 330 150 0 0 0 2>
  <GND * 5 330 460 0 0 0 0>
  <Eqn Eqn1 1 280 560 -28 15 0 0 "S21_dB=dB(S[2,1])" 1 "S11_dB=dB(S[1,1])" 1 "yes" 0>
  <Eqn Eqn2 1 500 560 -28 15 0 0 "S43_dB=dB(S[4,3])" 1 "S33_dB=dB(S[3,3])" 1 "yes" 0>
  <C C3 1 670 310 -26 -55 0 2 "220 pF" 1 "" 0 "neutral" 0>
  <.SP SP1 1 70 560 0 67 0 0 "log" 1 "200 MHz" 1 "1200 MHz" 1 "501" 1 "no" 0 "1" 0 "2" 0 "no" 0 "no" 0>
  <Pac P4 1 80 930 -103 -26 1 1 "3" 1 "100 Ohm" 1 "0 dBm" 0 "1 GHz" 0 "26.85" 0>
  <L L7 1 130 1060 10 -26 0 1 "100 nH" 1 "" 0>
  <L L8 1 130 810 10 -26 0 1 "100 nH" 1 "" 0>
  <C C8 1 190 840 -26 17 1 2 "4.3 pF" 1 "" 0 "neutral" 0>
  <C C9 1 190 1030 -26 -55 0 2 "4.3 pF" 1 "" 0 "neutral" 0>
  <GND *4 5 130 780 0 0 0 2>
  <GND *5 5 130 1090 0 0 0 0>
  <L L9 1 270 1030 -26 10 1 2 "43 nH" 1 "" 0>
  <L L10 1 270 840 -26 -48 1 0 "43 nH" 1 "" 0>
  <C C10 1 330 810 17 -26 0 1 "5.6 pF" 1 "" 0 "neutral" 0>
  <C C11 1 330 1060 17 -26 0 1 "5.6 pF" 1 "" 0 "neutral" 0>
  <GND *6 5 330 780 0 0 0 2>
  <GND *7 5 330 1090 0 0 0 0>
  <GND * 5 480 930 0 0 0 1>
  <Pac P3 1 540 920 18 -26 0 1 "4" 1 "50 Ohm" 1 "0 dBm" 0 "1 GHz" 0 "26.85" 0>
  <GND * 5 540 950 0 0 0 0>
  <SPfile X2 1 420 930 -71 -26 1 3 "hhm1591a2.s3p" 0 "rectangular" 0 "linear" 0 "open" 0 "3" 0>
  <GND * 5 430 460 0 0 0 0>
  <GND *1 5 510 310 0 0 0 1>
  <L L2 1 560 400 -26 10 1 2 "27 nH" 1 "" 0>
  <L L1 1 510 260 10 -26 0 1 "27 nH" 1 "" 0>
  <C C2 1 550 210 -26 -55 0 2 "5.1 pF" 1 "" 0 "neutral" 0>
  <C C1 0 510 360 17 -26 0 1 "5.1 pF" 1 "" 0 "neutral" 0>
  <C C13 1 430 430 17 -26 0 1 "11 pF" 1 "" 0 "neutral" 0>
  <C C12 1 500 860 -26 -55 0 2 "220 pF" 1 "" 0 "neutral" 0>
</Components>
<Wires>
  <80 400 130 400 "" 0 0 0 "">
  <80 330 80 400 "" 0 0 0 "">
  <80 210 130 210 "" 0 0 0 "">
  <80 210 80 270 "" 0 0 0 "">
  <130 400 160 400 "" 0 0 0 "">
  <130 210 160 210 "" 0 0 0 "">
  <220 400 240 400 "" 0 0 0 "">
  <220 210 240 210 "" 0 0 0 "">
  <750 310 750 330 "" 0 0 0 "">
  <700 310 750 310 "" 0 0 0 "">
  <300 400 330 400 "" 0 0 0 "">
  <300 210 330 210 "" 0 0 0 "">
  <330 400 430 400 "" 0 0 0 "">
  <80 1030 130 1030 "" 0 0 0 "">
  <80 960 80 1030 "" 0 0 0 "">
  <80 840 130 840 "" 0 0 0 "">
  <80 840 80 900 "" 0 0 0 "">
  <130 1030 160 1030 "" 0 0 0 "">
  <130 840 160 840 "" 0 0 0 "">
  <220 1030 240 1030 "" 0 0 0 "">
  <220 840 240 840 "" 0 0 0 "">
  <300 1030 330 1030 "" 0 0 0 "">
  <300 840 330 840 "" 0 0 0 "">
  <330 1030 390 1030 "" 0 0 0 "">
  <330 840 450 840 "" 0 0 0 "">
  <450 840 450 900 "" 0 0 0 "">
  <390 960 390 1030 "" 0 0 0 "">
  <390 860 390 900 "" 0 0 0 "">
  <540 860 540 890 "" 0 0 0 "">
  <390 860 470 860 "" 0 0 0 "">
  <530 860 540 860 "" 0 0 0 "">
  <330 210 510 210 "" 0 0 0 "">
  <630 310 640 310 "" 0 0 0 "">
  <590 400 630 400 "" 0 0 0 "">
  <630 310 630 400 "" 0 0 0 "">
  <510 210 510 230 "" 0 0 0 "">
  <510 290 510 310 "" 0 0 0 "">
  <510 210 520 210 "" 0 0 0 "">
  <630 210 630 310 "" 0 0 0 "">
  <580 210 630 210 "" 0 0 0 "">
  <510 310 510 330 "" 0 0 0 "">
  <510 400 530 400 "" 0 0 0 "">
  <510 390 510 400 "" 0 0 0 "">
  <430 400 510 400 "" 0 0 0 "">
</Wires>
<Diagrams>
</Diagrams>
<Paintings>
</Paintings>
